"""
File consists of several functions for the calculation rules of FAIR Quality KPIs
"""

from functions.classes import *

def test_function():
    """Test function to check module functionality"""
    print("You called the test function.")
#Initiierung der "Gesamtmasse-Funktion"
def kpi_mass(system: LegoAssembly)->float:
    """
    Calculates the total mass of the system

    Args:
        system (LegoAssembly): LegoAssembly object that represents the system

    Returns:
        total_mass (float): Sum of masses of all components in the system in g

    Raises:
        TypeError: If an argument with unsupported type is passed
            (i.e. anything other than LegoAssembly).
    """
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to kpi_mass()")
#Initiierung der Gesamtmasse
    total_mass = 0
    #Aufsummeriung aller Einzelmassen
    for c in system.get_component_list(-1):
        total_mass += c.properties["mass [g]"]
    #Rückgabe der Gesamtmasse in die Ausarbeitung    
    return total_mass

#Initiierung der "maximalen-Lieferzeit-Funktion"
def kpi_max_delivery_time(system: LegoAssembly)->float:
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to kpi_max_delivery_time()")
    #Erstellen eines leeren Array
    delivery_time = []
    #Eintragen jeder Lieferzeit jedes Bauteils in den Array
    for c in system.get_component_list(-1):
        delivery_time.append(c.properties["delivery time [days]"])
    #Suchen und Speichern der maximalen Lieferzeit eines Teils
    delivery_max = max(delivery_time)
    #Rückgabe der maximalen Lieferzeit in die Ausgabe
    return delivery_max

#Initiierung der "Gesamtkosten-Funktion"
def kpi_cost(system: LegoAssembly)->float:
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to kpi_cost()")
    cost = 0
    #Aufsummeriung aller Einzelmassen
    for c in system.get_component_list(-1):
        cost += c.properties["price [Euro]"]
    #Rückgabe der Gesamtkosten in die Ausarbeitung
    return cost
    


if __name__ == "__main__":
    """
    Function to inform that functions in this module is
    intended for import not usage as script
    """
    print(
        "This script contains functions for calculating the FAIR Quality KPIs."
        "It is not to be executed independently."
    )

  
